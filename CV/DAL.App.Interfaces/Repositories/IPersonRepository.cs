﻿using System;
using System.Threading.Tasks;
using DAL.EF;
using DAL.Interfaces;
using Domain;

namespace DAL.App.Interfaces.Repositories
{
    public interface IPersonRepository : IRepository<Person>
    {
        Task<Person> GetFirstPersonIncludeAll();
    }
}