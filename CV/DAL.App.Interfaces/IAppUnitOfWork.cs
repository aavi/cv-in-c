﻿using DAL.App.Interfaces.Repositories;
using DAL.Interfaces;
using Domain;

namespace DAL.App.Interfaces
{
    public interface IAppUnitOfWork : IUnitOfWork
    {
        IRepository<Accomplishment> Accomplishments { get; }
        IRepository<Company> Companies { get; }
        IRepository<Contact> Contacts { get; }
        IRepository<ContactType> ContactTypes { get; }
        IRepository<Education> Education { get; }
        IPersonRepository Persons { get; }
        IRepository<School> Schools { get; }
        IRepository<Skill> Skills { get; }
        IRepository<WorkExperience> WorkExperiences { get; }
        IRepository<Language> Languages { get; }
        IRepository<PersonLanguage> PersonLanguages { get; }
    }
}