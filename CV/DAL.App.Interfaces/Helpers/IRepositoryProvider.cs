﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using DAL.App.Interfaces;
using DAL.Interfaces;

namespace DAL.App.Interfaces.Helpers
{
    public interface IRepositoryProvider
    {
        IRepository<TEntity> GetEntityRepository<TEntity>() where TEntity : class;

        TRepository GetCustomRepository<TRepository>() where TRepository : class;
    }
}
