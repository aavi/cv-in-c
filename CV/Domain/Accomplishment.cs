﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    [DisplayName("Saavutus")]
    public class Accomplishment
    {
        public int AccomplishmentId { get; set; }

        [MaxLength(120)]
        [Display(Name = "Nimetus")]
        public string Title { get; set; }
        [MaxLength(500)]
        [Display(Name = "Kirjeldus")]
        public string Description { get; set; }

        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
    }
}