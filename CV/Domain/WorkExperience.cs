﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    [DisplayName("Töökogemus")]
    public class WorkExperience
    {
        public int WorkExperienceId { get; set; }
        
        [MaxLength(120)]
        [Display(Name = "Ametikoht")]
        public string Position { get; set; }
        [MaxLength(500)]
        [Display(Name = "Töökirjeldus")]
        public string Description { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Algus")]
        public DateTime StartDate { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Lõpp")]
        public DateTime EndDate { get; set; }

        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }
    }
}