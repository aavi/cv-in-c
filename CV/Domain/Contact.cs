﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    [DisplayName("Kontakt")]
    public class Contact
    {
        public int ContactId { get; set; }

        [MaxLength(250)]
        [Display(Name = "Kontakti sisu")]
        public string ContactValue { get; set; }

        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
        public int ContactTypeId { get; set; }
        public virtual ContactType ContactType { get; set; }
    }
}