﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Net.Security;
using Domain.Enums;

namespace Domain
{
    [DisplayName("Arvutioskus")]
    public class Skill
    {
        public int SkillId { get; set; }

        [MaxLength(120)]
        [Display(Name = "Tarkvara või keel")]
        public string Program { get; set; }
        [Display(Name = "Tase")]
        public SkillLevel SkillLevel { get; set; }

        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
    }
}