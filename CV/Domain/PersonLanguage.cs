﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Domain.Enums;

namespace Domain
{
    [DisplayName("Keeleoskus")]
    public class PersonLanguage
    {
        public int PersonLanguageId { get; set; }

        [Display(Name = "Kuulamine")]
        public LanguageSkillLevel ListeningSkillLevel { get; set; }
        [Display(Name = "Lugemine")]
        public LanguageSkillLevel ReadingSkillLevel { get; set; }
        [Display(Name = "Suuline suhtlus")]
        public LanguageSkillLevel SpeakingSkillLevel { get; set; }
        [Display(Name = "Suuline esitlus")]
        public LanguageSkillLevel OratorySkillLevel { get; set; }
        [Display(Name = "Kirjutamine")]
        public LanguageSkillLevel WritingSkillLevel { get; set; }

        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
        public int LanguageId { get; set; }
        public virtual Language Language { get; set; }
    }
}