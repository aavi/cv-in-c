﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    [DisplayName("Keel")]
    public class Language
    {
        public int LanguageId { get; set; }

        [MaxLength(120)]
        [Display(Name = "Keel")]
        public string LanguageName { get; set; }

        public virtual List<PersonLanguage> PersonLanguages { get; set; } = new List<PersonLanguage>();
    }
}