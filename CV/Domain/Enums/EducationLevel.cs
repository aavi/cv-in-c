﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Enums
{
    public enum EducationLevel
    {
        [Display(Name = "Muu")]
        Other = 0,
        [Display(Name = "Põhiharidus")]
        Basic = 1,
        [Display(Name = "Keskharidus")]
        Secondary = 2,
        [Display(Name = "Kutseharidus")]
        Vocational = 4,
        [Display(Name = "Kõrgharidus")]
        Higher = 8
    }
}