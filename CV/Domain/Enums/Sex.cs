﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain.Enums
{
    public enum Sex
    {
        [Display(Name = "Muu")]
        Other = 0,
        [Display(Name = "Mees")]
        Male = 1,
        [Display(Name = "Naine")]
        Female = 2
    }
}