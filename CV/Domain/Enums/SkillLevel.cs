﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Enums
{
    public enum SkillLevel
    {
        [Display(Name = "Puudub")]
        None = 0,
        [Display(Name = "Algtase")]
        Beginner = 1,
        [Display(Name = "Tavakasutaja")]
        Average = 2,
        [Display(Name = "Kesktase")]
        Medium = 4,
        [Display(Name = "Edasijõudnu")]
        Advanced = 8,
        [Display(Name = "Spetsialist")] 
        Specialist = 16
    }
}