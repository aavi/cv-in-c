﻿namespace Domain.Enums
{
    public enum LanguageSkillLevel
    {
        None = 0,
        A1 = 1,
        A2 = 2,
        B1 = 4,
        B2 = 8,
        C1 = 16,
        C2 = 32
    }
}