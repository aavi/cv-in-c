﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    [DisplayName("Kontaktitüüp")]
    public class ContactType
    {
        public int ContactTypeId { get; set; }

        [MaxLength(120)]
        [Display(Name = "Kontaktitüüp")]
        public string ContactTypeName { get; set; }

        public virtual List<Contact> Contacts { get; set; } = new List<Contact>();
    }
}