﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    [DisplayName("Ettevõte")]
    public class Company
    {
        public int CompanyId { get; set; }

        [MaxLength(120)]
        [Display(Name = "Ettevõtte nimi")]
        public string CompanyName { get; set; }

        public virtual List<WorkExperience> WorkExperiences { get; set; } = new List<WorkExperience>();
    }
}