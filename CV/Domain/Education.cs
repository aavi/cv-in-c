﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    [DisplayName("Haridus")]
    public class Education
    {
        public int EducationId { get; set; }

        [Display(Name = "Algus")]
        public int StartYear { get; set; }
        [Display(Name = "Lõpp")]
        public int? EndYear { get; set; }
        [Display(Name = "Lõpetatud")]
        public bool HasGraduated { get; set; } = true;
        [Display(Name = "Õpin veel")]
        public bool StillStudying { get; set; } = false;

        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
        public int SchoolId { get; set; }
        public virtual School School { get; set; }
    }
}