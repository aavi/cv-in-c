﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Domain.Enums;

namespace Domain
{
    [DisplayName("Kool")]
    public class School
    {
        public int SchoolId { get; set; }
        [MaxLength(120)]
        [Display(Name = "Õppeasutus")]
        public string SchoolName { get; set; }
        [Display(Name = "Haridus tase")]
        public EducationLevel EducationLevel { get; set; }

        public virtual List<Education> Educations { get; set; } = new List<Education>();
    }
}