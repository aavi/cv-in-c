﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Domain.Enums;

namespace Domain
{
    [DisplayName("Isik")]
    public class Person
    {
        public int PersonId { get; set; }

        [MaxLength(120)]
        [Display(Name = "Eesnimi")]
        public string Firstname { get; set; }
        [MaxLength(120)]
        [Display(Name = "Perenimi")]
        public string Lastname { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Sünniaeg")]
        public DateTime DateOfBirth { get; set; }
        [Display(Name = "Sugu")]
        public Sex Sex { get; set; }
        [Display(Name = "Täisnimi")]
        public string FirstLastName => Firstname + " " + Lastname;

        public virtual List<Contact> Contacts { get; set; } = new List<Contact>();
        public virtual List<WorkExperience> WorkExperiences { get; set; } = new List<WorkExperience>();
        public virtual List<Education> Educations { get; set; } = new List<Education>();
        public virtual List<Skill> Skills { get; set; } = new List<Skill>();
        public virtual List<Accomplishment> Accomplishments { get; set; } = new List<Accomplishment>();
        public virtual List<PersonLanguage> PersonLanguages { get; set; } = new List<PersonLanguage>();
    }
}