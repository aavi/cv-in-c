﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using DAL.App.EF;
using DAL.App.Interfaces;
using DAL.App.Interfaces.Helpers;
using DAL.App.Interfaces.Repositories;
using DAL.EF;
using DAL.Interfaces;
using Domain;

namespace DAL.App.EF
{
    public class AppEFUnitOfWork : IAppUnitOfWork
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IRepositoryProvider _repositoryProvider;

        public AppEFUnitOfWork(IDataContext dataContext, IRepositoryProvider repositoryProvider)
        {
            _repositoryProvider = repositoryProvider;
            _applicationDbContext = dataContext as ApplicationDbContext;
            if (_applicationDbContext == null)
            {
                throw new NullReferenceException("No EF dbcontext found in UOW");
            }
        }


        //public IPersonRepository People => GetCustomRepository<IPersonRepository>();

        public IRepository<Accomplishment> Accomplishments => GetEntityRepository<Accomplishment>();
        public IRepository<Company> Companies => GetEntityRepository<Company>();
        public IRepository<Contact> Contacts => GetEntityRepository<Contact>();
        public IRepository<ContactType> ContactTypes => GetEntityRepository<ContactType>();
        public IRepository<Education> Education => GetEntityRepository<Education>();
        public IPersonRepository Persons => GetCustomRepository<IPersonRepository>();
        public IRepository<School> Schools => GetEntityRepository<School>();
        public IRepository<Skill> Skills => GetEntityRepository<Skill>();
        public IRepository<WorkExperience> WorkExperiences => GetEntityRepository<WorkExperience>();
        public IRepository<Language> Languages => GetEntityRepository<Language>();
        public IRepository<PersonLanguage> PersonLanguages => GetEntityRepository<PersonLanguage>();

        public void SaveChanges()
        {
            _applicationDbContext.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _applicationDbContext.SaveChangesAsync();
        }

        public IRepository<TEntity> GetEntityRepository<TEntity>() where TEntity : class
        {
            return _repositoryProvider.GetEntityRepository<TEntity>();
        }

        public TRepositoryInterface GetCustomRepository<TRepositoryInterface>() where TRepositoryInterface : class
        {
            return _repositoryProvider.GetCustomRepository<TRepositoryInterface>();
        }
    }
}
