﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.App.EF;
using DAL.App.Interfaces.Helpers;
using DAL.App.Interfaces;
using DAL.App.Interfaces.Repositories;
using DAL.EF;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF.Repositories;

namespace DAL.App.EF.Helpers
{
    public class EFRepositoryFactory : IRepositoryFactory
    {
        private readonly Dictionary<Type, Func<IDataContext, object>> _customRepositoryFactories
            = GetCustomRepoFactories();

        private static Dictionary<Type, Func<IDataContext, object>> GetCustomRepoFactories()
        {
            return new Dictionary<Type, Func<IDataContext, object>>()
            {
                //{typeof(IPersonRepository), (datacontext) => new EFPersonRepository(datacontext as ApplicationDbContext) },
                {typeof(IPersonRepository), (datacontext) => new PersonRepository(datacontext as ApplicationDbContext) },

            };
        }

        public Func<IDataContext, object> GetCustomRepositoryFactory<TRepoInterface>() where TRepoInterface : class
        {
            _customRepositoryFactories.TryGetValue(
                typeof(TRepoInterface),
                out Func<IDataContext, object> factory
            );
            return factory;
        }

        public Func<IDataContext, object> GetStandardRepositoryFactory<TEntity>() where TEntity : class
        {

            return (datacontext) => new EFRepository<TEntity>(datacontext as ApplicationDbContext);
        }
    }
}
