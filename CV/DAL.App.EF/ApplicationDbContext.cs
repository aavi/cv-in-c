﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Interfaces;
using Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace DAL.App.EF
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IDataContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<Domain.Accomplishment> Accomplishments { get; set; }
        public DbSet<Domain.Company> Companies { get; set; }
        public DbSet<Domain.Contact> Contacts { get; set; }
        public DbSet<Domain.ContactType> ContactTypes { get; set; }
        public DbSet<Domain.Education> Educations { get; set; }
        public DbSet<Domain.Person> Persons { get; set; }
        public DbSet<Domain.School> Schools { get; set; }
        public DbSet<Domain.Skill> Skills { get; set; }
        public DbSet<Domain.WorkExperience> WorkExperiences { get; set; }
        public DbSet<Domain.Language> Languages { get; set; }
        public DbSet<Domain.PersonLanguage> PersonLanguages { get; set; }
        
    }
}
