﻿using System.Linq;
using System.Threading.Tasks;
using DAL.App.Interfaces.Repositories;
using DAL.EF;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class PersonRepository : EFRepository<Person>, IPersonRepository
    {
        public PersonRepository(DbContext datacontext) : base(datacontext)
        {
        }

        /// <summary>
        /// Returns the first Person in the DB along with all connected data if it can,
        /// else creates a new person and returns that instead. Used for populating the CV
        /// </summary>
        /// <returns></returns>
        public async Task<Person> GetFirstPersonIncludeAll()
        {
            // Create empty person to be returned in case the DB is empty
            Person dbPerson = await RepositoryDbSet
                .Include(x => x.Contacts)
                .ThenInclude(x => x.ContactType)
                .Include(x => x.WorkExperiences)
                .ThenInclude(x => x.Company)
                .Include(x => x.Educations)
                .ThenInclude(x => x.School)
                .Include(x => x.PersonLanguages)
                .ThenInclude(x => x.Language)
                .Include(x => x.Skills)
                .Include(x => x.Accomplishments)
                .FirstOrDefaultAsync();


            Person newPerson;
            if (dbPerson != null)
            {
                // Order all subLists
                dbPerson.Contacts = dbPerson.Contacts.OrderBy(x => x.ContactType.ContactTypeName)
                    .ThenBy(x => x.ContactValue).ToList();
                dbPerson.WorkExperiences = dbPerson.WorkExperiences.OrderBy(x => x.StartDate).ToList();
                dbPerson.Educations = dbPerson.Educations.OrderBy(x => x.StartYear).ToList();
                dbPerson.PersonLanguages = dbPerson.PersonLanguages.OrderBy(x => x.Language.LanguageName).ToList();
                dbPerson.Skills = dbPerson.Skills.OrderBy(x => x.Program).ToList();
                dbPerson.Accomplishments = dbPerson.Accomplishments.OrderBy(x => x.Title).ToList();

                newPerson = dbPerson;
            }
            else
                newPerson = new Person();

            return newPerson;
        }

    }
}