﻿using DAL.App.Interfaces;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Enums;

namespace Helpers
{
    public class DbReinitializer
    {
        private readonly IAppUnitOfWork _uow;

        public DbReinitializer(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        private async Task ClearDb()
        {
            _uow.Accomplishments.Clear();
            _uow.Skills.Clear();
            _uow.Education.Clear();
            _uow.Schools.Clear();
            _uow.PersonLanguages.Clear();
            _uow.Languages.Clear();
            _uow.Contacts.Clear();
            _uow.ContactTypes.Clear();
            _uow.WorkExperiences.Clear();
            _uow.Companies.Clear();
            _uow.Persons.Clear();
            await _uow.SaveChangesAsync();
        }

        public async Task ReinitializeDatabase()
        {
            await ClearDb();

            var person = new Person
            {
                Firstname = "Alo",
                Lastname = "Avi",
                DateOfBirth = new DateTime(1988, 9, 16),
                Sex = Sex.Male
            };
            await _uow.Persons.AddAsync(person);

            var typeEmail = new ContactType { ContactTypeName = "Email" };
            await _uow.ContactTypes.AddAsync(typeEmail);
            var typePhone = new ContactType { ContactTypeName = "Telefon" };
            await _uow.ContactTypes.AddAsync(typePhone);
            var typeAddress = new ContactType { ContactTypeName = "Elukoht" };
            await _uow.ContactTypes.AddAsync(typeAddress);

            var companyAbc = new Company { CompanyName = "ABC Supermarkets AS" };
            await _uow.Companies.AddAsync(companyAbc);
            var companyRimi = new Company { CompanyName = "Rimi Eesti Food AS" };
            await _uow.Companies.AddAsync(companyRimi);

            var schoolRidala = new School()
            {
                SchoolName = "Ridala Põhikool",
                EducationLevel = EducationLevel.Basic
            };
            await _uow.Schools.AddAsync(schoolRidala);
            var schoolHg = new School
            {
                SchoolName = "Haapsalu Gümnaasium",
                EducationLevel = EducationLevel.Secondary
            };
            await _uow.Schools.AddAsync(schoolHg);
            var schoolIt = new School
            {
                SchoolName = "IT Kolledž",
                EducationLevel = EducationLevel.Higher
            };
            await _uow.Schools.AddAsync(schoolIt);

            var langEst = new Language { LanguageName = "Eesti keel" };
            await _uow.Languages.AddAsync(langEst);
            var langEn = new Language { LanguageName = "Inglise keel" };
            await _uow.Languages.AddAsync(langEn);
            var langGer = new Language { LanguageName = "Saksa keel" };
            await _uow.Languages.AddAsync(langGer);

            await _uow.Contacts.AddAsync(new Contact()
            {
                Person = person,
                ContactType = typeEmail,
                ContactValue = "aavi@itcollege.ee"
            });
            await _uow.Contacts.AddAsync(new Contact()
            {
                Person = person,
                ContactType = typePhone,
                ContactValue = "5332 1779"
            });
            await _uow.Contacts.AddAsync(new Contact()
            {
                Person = person,
                ContactType = typeAddress,
                ContactValue = "Endla 74-2,10613 Tallinn Eesti"
            });

            await _uow.WorkExperiences.AddAsync(new WorkExperience()
            {
                Person = person,
                Company = companyAbc,
                StartDate = new DateTime(2013, 5, 1),
                EndDate = new DateTime(2016, 11, 1),
                Position = "Kauba vastuvõtja",
                Description = "Kauba vastuvõtt, kontroll ning korrektsesse osakonda toimetamine."
            });
            await _uow.WorkExperiences.AddAsync(new WorkExperience()
            {
                Person = person,
                Company = companyRimi,
                StartDate = new DateTime(2011, 10, 1),
                EndDate = new DateTime(2013, 5, 1),
                Position = "Müügispetsialist",
                Description = "Alkoholi osakonna korrashoid, kauba tellimine, vastuvõtt. Klientide probleemide lahendus. Müüjate töö juhendamine järelvalve."
            });
            await _uow.WorkExperiences.AddAsync(new WorkExperience()
            {
                Person = person,
                Company = companyRimi,
                StartDate = new DateTime(2009, 6, 1),
                EndDate = new DateTime(2011, 10, 1),
                Position = "Müüja",
                Description = "Kliendi teenindus nii kassas kui saali peal. Kauba tellimine ning väljapanek. Lisaks vajatuse korral inventuurid ja koristamine."
            });

            await _uow.Education.AddAsync(new Education()
            {
                Person = person,
                School = schoolRidala,
                StartYear = 1996,
                EndYear = 2005,
                HasGraduated = true,
                StillStudying = false
            });
            await _uow.Education.AddAsync(new Education()
            {
                Person = person,
                School = schoolHg,
                StartYear = 2005,
                EndYear = 2007,
                HasGraduated = true,
                StillStudying = false
            });
            await _uow.Education.AddAsync(new Education()
            {
                Person = person,
                School = schoolIt,
                StartYear = 2016,
                HasGraduated = false,
                StillStudying = true
            });

            await _uow.PersonLanguages.AddAsync(new PersonLanguage()
            {
                Person = person,
                Language = langEst,
                ListeningSkillLevel = LanguageSkillLevel.C2,
                ReadingSkillLevel = LanguageSkillLevel.C2,
                SpeakingSkillLevel = LanguageSkillLevel.C1,
                OratorySkillLevel = LanguageSkillLevel.C1,
                WritingSkillLevel = LanguageSkillLevel.C1
            });
            await _uow.PersonLanguages.AddAsync(new PersonLanguage()
            {
                Person = person,
                Language = langEn,
                ListeningSkillLevel = LanguageSkillLevel.C2,
                ReadingSkillLevel = LanguageSkillLevel.C2,
                SpeakingSkillLevel = LanguageSkillLevel.C1,
                OratorySkillLevel = LanguageSkillLevel.C1,
                WritingSkillLevel = LanguageSkillLevel.C1
            });
            await _uow.PersonLanguages.AddAsync(new PersonLanguage()
            {
                Person = person,
                Language = langGer,
                ListeningSkillLevel = LanguageSkillLevel.B2,
                ReadingSkillLevel = LanguageSkillLevel.B1,
                SpeakingSkillLevel = LanguageSkillLevel.A2,
                OratorySkillLevel = LanguageSkillLevel.A1,
                WritingSkillLevel = LanguageSkillLevel.A1
            });

            await _uow.Skills.AddAsync(new Skill()
            {
                Person = person,
                Program = "C#",
                SkillLevel = SkillLevel.Average
            });
            await _uow.Skills.AddAsync(new Skill()
            {
                Person = person,
                Program = "Java",
                SkillLevel = SkillLevel.Average
            });
            await _uow.Skills.AddAsync(new Skill()
            {
                Person = person,
                Program = "JavaScript",
                SkillLevel = SkillLevel.Average
            });
            await _uow.Skills.AddAsync(new Skill()
            {
                Person = person,
                Program = "Python",
                SkillLevel = SkillLevel.Beginner
            });
            await _uow.Skills.AddAsync(new Skill()
            {
                Person = person,
                Program = "C",
                SkillLevel = SkillLevel.Beginner
            });
            await _uow.Skills.AddAsync(new Skill()
            {
                Person = person,
                Program = "C++",
                SkillLevel = SkillLevel.Beginner
            });
            await _uow.Skills.AddAsync(new Skill()
            {
                Person = person,
                Program = "SQL",
                SkillLevel = SkillLevel.Beginner
            });
            await _uow.Skills.AddAsync(new Skill()
            {
                Person = person,
                Program = "MS Word",
                SkillLevel = SkillLevel.Advanced
            });
            await _uow.Skills.AddAsync(new Skill()
            {
                Person = person,
                Program = "MS Excel",
                SkillLevel = SkillLevel.Advanced
            });
            await _uow.Skills.AddAsync(new Skill()
            {
                Person = person,
                Program = "MS PowerPoint",
                SkillLevel = SkillLevel.Average
            });
            await _uow.Skills.AddAsync(new Skill()
            {
                Person = person,
                Program = "PHP",
                SkillLevel = SkillLevel.Beginner
            });
            await _uow.Skills.AddAsync(new Skill()
            {
                Person = person,
                Program = "Võrguseadmed",
                SkillLevel = SkillLevel.Average
            });

            await _uow.Accomplishments.AddAsync(new Accomplishment()
            {
                Person = person,
                Title = "Laphack 2017",
                Description = "Osalesin hackatonil. Meie meeskond sai Ericssoni eriauhinna."
            });

            await _uow.SaveChangesAsync();
        }
    }
}
