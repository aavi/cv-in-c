﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;

namespace WebApp.Controllers
{
    public class PersonLanguagesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PersonLanguagesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PersonLanguages
        public async Task<IActionResult> Index()
        {
            var ApplicationDbContext = _context.PersonLanguages.Include(p => p.Language).Include(p => p.Person);
            return View(await ApplicationDbContext.ToListAsync());
        }

        // GET: PersonLanguages/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personLanguage = await _context.PersonLanguages
                .Include(p => p.Language)
                .Include(p => p.Person)
                .SingleOrDefaultAsync(m => m.PersonLanguageId == id);
            if (personLanguage == null)
            {
                return NotFound();
            }

            return View(personLanguage);
        }

        // GET: PersonLanguages/Create
        public IActionResult Create()
        {
            ViewData["LanguageId"] = new SelectList(_context.Languages, "LanguageId", nameof(Language.LanguageName));
            ViewData["PersonId"] = new SelectList(_context.Persons, "PersonId", nameof(Person.FirstLastName));
            return View();
        }

        // POST: PersonLanguages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PersonLanguageId,ListeningSkillLevel,ReadingSkillLevel,SpeakingSkillLevel,OratorySkillLevel,WritingSkillLevel,PersonId,LanguageId")] PersonLanguage personLanguage)
        {
            if (ModelState.IsValid)
            {
                _context.Add(personLanguage);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["LanguageId"] = new SelectList(_context.Languages, "LanguageId", nameof(Language.LanguageName), personLanguage.LanguageId);
            ViewData["PersonId"] = new SelectList(_context.Persons, "PersonId", nameof(Person.FirstLastName), personLanguage.PersonId);
            return View(personLanguage);
        }

        // GET: PersonLanguages/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personLanguage = await _context.PersonLanguages.SingleOrDefaultAsync(m => m.PersonLanguageId == id);
            if (personLanguage == null)
            {
                return NotFound();
            }
            ViewData["LanguageId"] = new SelectList(_context.Languages, "LanguageId", nameof(Language.LanguageName), personLanguage.LanguageId);
            ViewData["PersonId"] = new SelectList(_context.Persons, "PersonId", nameof(Person.FirstLastName), personLanguage.PersonId);
            return View(personLanguage);
        }

        // POST: PersonLanguages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PersonLanguageId,ListeningSkillLevel,ReadingSkillLevel,SpeakingSkillLevel,OratorySkillLevel,WritingSkillLevel,PersonId,LanguageId")] PersonLanguage personLanguage)
        {
            if (id != personLanguage.PersonLanguageId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(personLanguage);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PersonLanguageExists(personLanguage.PersonLanguageId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["LanguageId"] = new SelectList(_context.Languages, "LanguageId", nameof(Language.LanguageName), personLanguage.LanguageId);
            ViewData["PersonId"] = new SelectList(_context.Persons, "PersonId", nameof(Person.FirstLastName), personLanguage.PersonId);
            return View(personLanguage);
        }

        // GET: PersonLanguages/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personLanguage = await _context.PersonLanguages
                .Include(p => p.Language)
                .Include(p => p.Person)
                .SingleOrDefaultAsync(m => m.PersonLanguageId == id);
            if (personLanguage == null)
            {
                return NotFound();
            }

            return View(personLanguage);
        }

        // POST: PersonLanguages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var personLanguage = await _context.PersonLanguages.SingleOrDefaultAsync(m => m.PersonLanguageId == id);
            _context.PersonLanguages.Remove(personLanguage);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PersonLanguageExists(int id)
        {
            return _context.PersonLanguages.Any(e => e.PersonLanguageId == id);
        }
    }
}
