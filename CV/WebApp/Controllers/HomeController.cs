﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DAL.App.EF;
using DAL.App.Interfaces;
using Domain;
using Helpers;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAppUnitOfWork _uow;
        private readonly DbReinitializer _reinitializer;

        public HomeController(IAppUnitOfWork uow)
        {
            _uow = uow;
            _reinitializer = new DbReinitializer(_uow);
        }

        public async Task<IActionResult> Index()
        {
            var model = await _uow.Persons.GetFirstPersonIncludeAll();

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Index(Person model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                await _reinitializer.ReinitializeDatabase();
            }
            return RedirectToAction(actionName: nameof(Index));
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
