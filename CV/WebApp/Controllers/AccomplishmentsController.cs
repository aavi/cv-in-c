﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using DAL.App.Interfaces;
using Domain;

namespace WebApp.Controllers
{
    public class AccomplishmentsController : Controller
    {
        private readonly ApplicationDbContext _context;
        //private readonly IAppUnitOfWork _uow; //TODO: Change Controllers over to UoW

        public AccomplishmentsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Accomplishments
        public async Task<IActionResult> Index()
        {
            var ApplicationDbContext = _context.Accomplishments.Include(a => a.Person);
            return View(await ApplicationDbContext.ToListAsync());
        }

        // GET: Accomplishments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var accomplishment = await _context.Accomplishments
                .Include(a => a.Person)
                .SingleOrDefaultAsync(m => m.AccomplishmentId == id);
            if (accomplishment == null)
            {
                return NotFound();
            }

            return View(accomplishment);
        }

        // GET: Accomplishments/Create
        public IActionResult Create()
        {
            ViewData["PersonId"] = new SelectList(_context.Persons, "PersonId", nameof(Person.FirstLastName));
            return View();
        }

        // POST: Accomplishments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AccomplishmentId,Title,Description,PersonId")] Accomplishment accomplishment)
        {
            if (ModelState.IsValid)
            {
                _context.Add(accomplishment);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PersonId"] = new SelectList(_context.Persons, "PersonId", nameof(Person.FirstLastName), accomplishment.PersonId);
            return View(accomplishment);
        }

        // GET: Accomplishments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var accomplishment = await _context.Accomplishments.SingleOrDefaultAsync(m => m.AccomplishmentId == id);
            if (accomplishment == null)
            {
                return NotFound();
            }
            ViewData["PersonId"] = new SelectList(_context.Persons, "PersonId", nameof(Person.FirstLastName), accomplishment.PersonId);
            return View(accomplishment);
        }

        // POST: Accomplishments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AccomplishmentId,Title,Description,PersonId")] Accomplishment accomplishment)
        {
            if (id != accomplishment.AccomplishmentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(accomplishment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AccomplishmentExists(accomplishment.AccomplishmentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PersonId"] = new SelectList(_context.Persons, "PersonId", nameof(Person.FirstLastName), accomplishment.PersonId);
            return View(accomplishment);
        }

        // GET: Accomplishments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var accomplishment = await _context.Accomplishments
                .Include(a => a.Person)
                .SingleOrDefaultAsync(m => m.AccomplishmentId == id);
            if (accomplishment == null)
            {
                return NotFound();
            }

            return View(accomplishment);
        }

        // POST: Accomplishments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var accomplishment = await _context.Accomplishments.SingleOrDefaultAsync(m => m.AccomplishmentId == id);
            _context.Accomplishments.Remove(accomplishment);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AccomplishmentExists(int id)
        {
            return _context.Accomplishments.Any(e => e.AccomplishmentId == id);
        }
    }
}
