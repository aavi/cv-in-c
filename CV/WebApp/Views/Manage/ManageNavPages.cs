﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace WebApp.Views.Manage
{
    public static class ManageNavPages
    {
        public static string ActivePageKey => "ActivePage";

        public static string Index => "Index";

        public static string ChangePassword => "ChangePassword";

        public static string ExternalLogins => "ExternalLogins";

        public static string TwoFactorAuthentication => "TwoFactorAuthentication";

        public static string IndexNavClass(ViewContext viewcontext) => PageNavClass(viewcontext, Index);

        public static string ChangePasswordNavClass(ViewContext viewcontext) => PageNavClass(viewcontext, ChangePassword);

        public static string ExternalLoginsNavClass(ViewContext viewcontext) => PageNavClass(viewcontext, ExternalLogins);

        public static string TwoFactorAuthenticationNavClass(ViewContext viewcontext) => PageNavClass(viewcontext, TwoFactorAuthentication);
    
        public static string PageNavClass(ViewContext viewcontext, string page)
        {
            var activePage = viewcontext.ViewData["ActivePage"] as string;
            return string.Equals(activePage, page, StringComparison.OrdinalIgnoreCase) ? "active" : null;
        }

        public static void AddActivePage(this ViewDataDictionary viewData, string activePage) => viewData[ActivePageKey] = activePage;
    }
}
